package com.horay.ch01.code01;

import java.util.UUID;

/**
 * 获取最大公约数
 *
 * Created by heyingcheng on 2017/8/2.
 */
public class GCD {

	public int gcd(int p, int q) {
		if (q == 0) {
			return p;
		}
		int r = p % q;
		return gcd(q, r);
	}

	public static void main(String[] args) {
		System.out.println(UUID.randomUUID().toString());
	}

}
