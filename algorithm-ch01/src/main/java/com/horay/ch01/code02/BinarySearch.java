package com.horay.ch01.code02;

import lombok.extern.slf4j.Slf4j;

/**
 * @category 二分查找
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/2/5 10:37
 */
@Slf4j
public class BinarySearch {

    public int rank(int key, int[] array) {
        int lo = 0;
        int hi = array.length - 1;
        while (lo <= hi) {
            int mid = lo + ((hi - lo) / 2);
            log.info("mid=" + mid);
            if (key > array[mid]) {
                lo = mid + 1;
            } else if (key < array[mid]) {
                hi = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

}
