package com.horay.ch01.code01;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @category GCD单元测试类
 * @author heyingcheng
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/2/5 10:16
 */
@Slf4j
public class GCDTest {

    @Test
    public void testGCD() {
        GCD gcd = new GCD();
        int result = gcd.gcd(7, 10);
        log.info(String.valueOf(result));
    }

}