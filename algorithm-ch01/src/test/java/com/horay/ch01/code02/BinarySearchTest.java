package com.horay.ch01.code02;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author heyingcheng
 * @category Class description
 * @email heyingcheng@ctvit.com.cn
 * @date 2018/2/5 10:42
 */
@Slf4j
public class BinarySearchTest {

    @Test
    public void testRank() {
        BinarySearch bs = new BinarySearch();
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        for (int i = 0, length = array.length; i < length; i++) {
            int rand = bs.rank(i, array);
            Assert.assertEquals(rand, array[i]);
        }
    }

}